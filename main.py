import socketserver
import time
import threading

class ThreadedHandler(socketserver.BaseRequestHandler):

    cool_dict = {}
    lock = threading.Lock()

    def set_cool_dict(self, keyy, vall):
        ThreadedHandler.cool_dict[keyy] = vall
        return 'ok'

    def get_cool_dict(self, keyy):
        return ThreadedHandler.cool_dict[keyy]

    def inc_cool_dict(self, keyy, vall):
        with ThreadedHandler.lock:
            time.sleep(5)
            try:
                prev = int(ThreadedHandler.cool_dict[keyy])
                ThreadedHandler.cool_dict[keyy] = prev + int(vall)
                return f'{prev}, {ThreadedHandler.cool_dict[keyy]}'
            except KeyError:
                return 'No such key'
            except:
                return "invalid integer in key or val"

    def dec_cool_dict(self, keyy, vall):
        with ThreadedHandler.lock:
            time.sleep(5)
            try:
                prev = int(ThreadedHandler.cool_dict[keyy])
                ThreadedHandler.cool_dict[keyy] = prev - int(vall)
                return f'{prev}, {ThreadedHandler.cool_dict[keyy]}'
            except KeyError:
                return 'No such key'
            except:
                return "invalid integer in key or val"

    op_dict = {
        'set': set_cool_dict,
        'get': get_cool_dict,
        'inc': inc_cool_dict,
        'dec': dec_cool_dict,
    }

    def handle(self):
        while True:
            raw = self.request.recv(2048)
            if not raw:
                break
            current_thread = threading.current_thread()
            data = raw.decode("utf-8").rstrip().split(' ')

            func = ThreadedHandler.op_dict[data[0]]
            params = data[1:]
            print(params)

            response = f"{current_thread}: {func(self, *params)}"
            self.request.sendall(response.encode("utf-8"))

ADDRESS = ("127.0.0.1", 4040)
print(f"Start server on {ADDRESS}")

server = socketserver.ThreadingTCPServer(
    ADDRESS,
    ThreadedHandler
)
with server:
    server.serve_forever()